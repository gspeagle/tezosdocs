.. _how_to_get:

-----------------------------

How To Get Tezos
==============================================================

This “How To” will explain the process of connecting and running a node on any of the three  Tezos networks. We recommend that you run a node via docker images. These are automatically generated from the GitLab repository and published on `DockerHub <https://hub.docker.com/r/tezos/tezos/>`_.

Currently, the requirements are a working installation of Docker on a machine with
X86_64 architecture. Although officially we support and develop on Linux via Docker, the script has been tested with success on both Mac and Windows machines. This documentation will give both build instructions for Docker and compilation for sources that will apply to most Linux distros.



Build from Sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here we will provide both a TL;DR list of commands, as well as a more detailed walkthrough.

Debian, Ubuntu, and Centros distros require the bubblewrap package to compile Tezos.

Install bubblewrap and the latest version of git -Ubuntu 16

::

   sudo add-apt-repository ppa:ansible/bubblewrap
   sudo add-apt-repository ppa:git-core/ppa
   sudo apt-get update

Similarly, if you are on Debian 9, use the following command to upgrade git to version 2.18.0.
You’ll need the Debian "stretch-backports" source enabled to do this. If the following command fails, follow this link ["Add backports to your sources.list"] and try it again.

::

   sudo apt-get -t stretch-backports install git

If using Centos 7.5, you’ll find that some of the system packages have different names. Instead of “apt-get” use the following commands:

::

   sudo yum install epel-release
   sudo yum install bzip2 screen wget rsync gcc m4 make unzip patch libev-devel  libev
   hidapi-devel gmp-devel bubblewrap git

Additionally, if bubblewrap isn’t available in your distribution, you can also skip it and init opam with

::

   --disable-sandbox.


Environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tezos is developed for Linux x86_64, primarily for Debian/Ubuntu and Archlinux. The protocol has limited operability on macOS/x86_64; Linux/armv7h (32 bits) (Raspberry Pi3, etc.), and Linux/aarch64 (64 bits) (Raspberry Pi3, etc.). A Windows port is feasible.




Get the sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All development happens at the Tezos git repository hosted at GitLab.

**Do not use the inactive GitHub mirror.**

Note: For most Linux distros, the quickest build is to install opam before cloning the Tezos repository and running a node.




Install OPAM (OCaml Package Manager)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To compile Tezos, you need the OPAM package manager, version 2.0.0. This is the most current version of OPAM, which our build script will always use. The build script will install and setup the correct version of the OPAM compiler on your machine. It will also set up OPAM, download the right version of the OCaml compiler, and so on. Alternative installation steps are also available.

Depending on your distro, there are a few different ways to install OPAM: here are a few from our community that have worked.

Opam installation method 1:
If asked, just accept the default of installing to /usr/local/bin. Installing there depends on the 'tezos' user having sudo rights as we arranged above.
::

   sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/ install.sh)


Opam installation method 2:
::

   wget https://github.com/ocaml/opam/releases/download/2.0.3/opam-2.0.3-x86_64-linux


Opam installation method for Mac OS:
::

   wget https://github.com/ocaml/opam/releases/download/2.0.3/opam-2.0.3-x86_64-linux
   sudo mv opam-2.0.0-rc3-x86_64-darwin /usr/local/bin/opam
   sudo chmod a+x /usr/local/bin/opam
   echo "0e7204eb98e66f594d33814051247d95fc9309a61b418d0a5c15468efcdf5501  /usr/local/bin/opam"|shasum -a 256 -c


Now that opam is installed, initialize it. When the following runs, allow it to update your .profile and, if asked, also allow to "add a hook to opam's init scripts". The init step will take quite a while to complete.

::

   opam init --bare
   eval $(opam env)


If you are running in the Windows 10 Linux Subsystem you may need to add > --disable-sandboxing to the call to opam init above. Otherwise you may be blocked by brwap errors as bubblewrap does not currently work in that environment.


Get the alphanet source code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   git clone -b alphanet https://gitlab.com/tezos/tezos.git
   cd tezos


Compile OPAM Switch and Build Dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   make build-deps

This command creates a local OPAM switch, through which the correct version of OCaml is compiled and installed. This can take a long time.

After compiling OCaml, OPAM will build the Tezos dependencies. OPAM is able to correctly support the OCaml libraries but cannot manage all external C libraries we depend on. OPAM will at the very least initiate a call to the system package manager but will not verify or perform a version check. For other issues related to architecture specific dependencies, we recommend you visit the very thorough, helpful, community-curated github page.

Once the dependencies are built, update the environment (1) so as to refer to the new switch and then compile the Tezos project (2)

::

   eval $(opam env)
   make



TL, DR Command List
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   apt-get update
   apt-get dist-upgrade -y
   adduser tezos
   adduser tezos sudo
   reboot
   sudo apt-get install -y patch unzip make gcc m4 git g++ aspcud bubblewrap curl bzip2 rsync libev-dev libgmp-dev pkg-config libhidapi-dev
   wget https://github.com/ocaml/opam/releases/download/2.0.3/opam-2.0.3-x86_64-linux 
   opam init --bare
   eval $(opam env)
   git clone -b alphanet https://gitlab.com/tezos/tezos.git
   cd tezos
   make build-deps
   eval $(opam env)
   make


Docker Images
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following set of commands will install and run a Tezos node from your machine via a working installation of Docker.

The script ``alphanet.sh`` is provided to help download the correct and most up to date image for the network. In this instance the script will be used to install and run an alphanet node on your machine. If you would like to run either the Zeronet or Mainnet, then simply replace “alphanet.sh”  with mainnet.sh or zeronet.sh, depending on which network you would like to run a node.

For example, to run Alphanet:

::

    wget https://gitlab.com/tezos/tezos/raw/master/scripts/alphanet.sh
    chmod +x alphanet.sh

Alternatively, to run Mainnet:

::

    wget -O mainnet.sh https://gitlab.com/tezos/tezos/raw/master/scripts/alphanet.sh
    chmod +x mainnet.sh

All of our illustrative examples assume you are running an Alphanet node. Please be advised we strongly urge users to start interacting with the Tezos protocol on one of the testnets before transaction and interaction with the Mainnet.

To begin running a node, simply run the following command

::

   ./alphanet.sh start

This will download the correct docker image for your chosen corresponding network and will initiate the launch of three docker containers: One running the node, one the Baker, and one the Endorser. (Baker and Endorser are covered later in this documentation). The initial download and launch takes a variable amount of time depending on which network you use to download the docker images and synchronize the chain.

Every call to ``alphanet.sh`` will check for updates of the Tezos node. The call will fail if your node is not up-to-date. To update the node, use the following:

::

   ./alphanet.sh restart

To temporarily disable automatic updates, you need only set the following environment variable:

::

   export TEZOS_ALPHANET_DO_NOT_PULL=yes)

See ``./alphanet.sh --help`` for more informations about the script. In particular see ``./alphanet.sh client --help``

The online client manual is a valuable resource as well. [http://tezos.gitlab.io/master/api/cli-commands.html#client-manual]

Every command to the ``tezos-client`` can be equivalently executed using ``./alphanet.sh client``.



