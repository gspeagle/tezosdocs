.. _how_to_use:

-----------------------------

How To Use Tezos
==================================================

This “How To” will cover the baisc interactions and transactions with the Tezos network. As well as covering the individual binaries that were compiled in the preceeding section This sections assumes that Tezos and OPAM have been successfully built and compiled, which was covered in the preceding section.


The Binaries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Upon the succesful compliation you will have the following binary packages:

``tezos-node``: the tezos node itself

``tezos-client``: a command-line client and basic wallet

``tezos-admin-client``: administration tool for the node

``tezos-{baker,endorser,accuser}-003_PsddFKi3``:   scripts to bake, endorse and accuse on the Tezos network (see :ref:`howtorun`)

``tezos-signer``: a client to remotely sign operations or block (see :ref:`signer`)

Note that Alphanet and Zeronet only support the latest version of the protocol while the Mainnet  must also support all past protocols.

Tezos is currently in protocol : 003_PsddFKi3,  as the network is updated and new developments pushed, this protocol version will change.  The quickest route for information on protocol changes would be to connect with the community via the Baker's Slack Room.  You can request and invite by emailing one of our partners at Obsidian Systems.  



The Manual Is Your Friend!!!!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The manual of each binary package can be viewed with the command following command ``man`` 
and much more specific details can be found by passing the  ``-v``  option after the ``man`` command to increase verbosity.
 
 To use one specific command, type the command without arguments to see possible completions and options. It is also possible to search a keyword in the manual with ``man keyword``. The full documentation is also available online :ref:`client_manual`.

::

   tezos-client man -v 3
   tezos-client transfer
   tezos-client man set


The Tezos Node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Tezos node is both a vital and necessary component to the Tezos Network.  It connects to other nodes, thus providing the support and structure to the decentralized Tezos blockchain.  Communication and "gossip" between nodes is the backbone of any blockchain and most distributed systems. The gossip network facilitates the sharing of operations and block context between nodes.   In addition to communicating with each other, the nodes also store and update the data (context)  contained within the blocks.   These blocks play the role of the immutable ledger for the Tezos network and this is one of the aspects that makes blockchain and decentralized systems so attractive in the quest for transperancy and efficienty in our  both electronic and public administration systems.  The Tezos Node is the anchor and scaffolding upon which the larger Tezos network is stabilized and built upon. 

The context` is the full state of the blockchain shared by all the nodes (peers). Approximately every minute a new block baked. Baking and Staking will be covered in length in this section "LINK" , When the shell receives the new block, it applies each operation in the block to its current
context and computes a new context. When a block is received, the data and operations contained therein represent an updated state of the blockchain. This changes the context, and the new context is broadcast via the peer to peer network.  The most recent block baked and broadcast is also called the head of that specific chain. Each new head is broadcast by the node to its peers, disseminating this information to build a consensus across the network on the new state of context of the blockchain.

Other than passively observing the network, your node can also inject its own new operations into a block when instructed by the ``tezos-client``.  In addition, it is possible to bake and broadcast new blocks when guided by the ``tezos-baker-003_PsddFKi3`` These specific functions and operations will be covered in this documentaiton in subsequent sections.

The node also has the capability to monitor the multiple chains that may exist concurrently and selects the best of the mulitple chains one based on its fitness (see :ref:`proof-of-stake`).


Node Identity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After a succesful build,  a new identity must be generated for your node in order to interact with the Tezos network. From a terminal run the following command: 

::

    tezos-node identity generate


Your node identity consists of  a pair of cryptographic keys that are used to encrypt messages sent to other nodes, and an antispam-Proof-of-Work stamp ensuring that enough computing power has been dedicated to creating the identity to prevent a malicious attempt to disrput the network of nodes. 

Note that although this a necessary step in interacting with the Tezos protocol, this is merely a network identity and is not related in any way to a Tezos address on the blockchain.

Blockchain Data Storage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All blockchain data is stored under ``$HOME/.tezos-node/``. If for some reason your node is acting erratically or there has been an upgrade to the network, it is safe to remove this directory. However, this will cause likely result in a long delay while re-syncing your node with the blockchain data.   

Also contained in the ``$HOME/.tezos-node/`` folder is the ``context`` ``identity.json`` ``peers.json`` ``store`` and the ``version.json`` files and directories.

There are resources currently available in the community that facilitate a much faster sync with the blockchain  (www.tezosdutch.com)

You can retain the  ``identity.json`` file if it takes a long time for you to compute the identity with the 
::
    tezos-node idenitity generate

command and only remove the ``store`` and ``context`` directories.


If you are also running the baker script, make sure that it has access to the ``.tezos-node`` directory.  This ensures accurate and 


RPC (Remote Procedure Call)  Interface
~~~~~~~~~~~~~

IN order to interface with the node you will need to enable JSON RPC capabitlites on your machine, which is disabled by default.



The RPC interface must be enabled in order for the ``tezos-client`` to communicate with the node, but it should not be publicly accessible on the internet, in order to thwart any potential security vulnerabilities. 

With the following command, you can run your node through the ``localhost`` address of your machine and on the default port ``8732``.

::

   tezos-node run --rpc-addr 127.0.0.1:8732 

The node listens by default on port ``8732`` so it is advisable to open incoming connections to that port. 

You can read more about RPCs, node configuration, and running in private mode at the following links

:ref:`node configuration <node-conf>` :ref:`private mode <private-mode>`  :ref:`RPC index. <rpc>`


The Tezos Client
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Most of your interactionns with the Tezos Node will be via command line in the ``tezos-client`` package from your terminal.  Creating addresses, simple wallets, delegating, and transferring are just a few of the operations possible when interacting with the Tezos Blockchain from the ``tezos-client``

The ``tezos-client`` is utilized in interactions with the node; It is possible to query its own status or command the node to perform some actions.

For example, after starting your node you can check if it has finished synchronizing using the following command

::

   tezos-client bootstrapped

This call will return with a timestamp, based on how quickly your node is syncing with the Tezos Blockchain.  As the network data is received from the other nodes, the timestamp returned via the ``tezos-client bootstrapped`` command will get closer to the current date and time (UTC).  Once the node is fully synched, the command will return with data consistent with the data and time from which is it was run.  

We can now check what is the current timestamp of the head of the chain (time is in UTC so it may differ from your local):

::

   tezos-client get timestamp


A Simple Wallet (Your Cryptographic Identity and Addresses) 
~~~~~~~~~~~~~~~

NOTE:  ALTHOUGH THESE COMMANDS AND INSTRUCTIONS ARE VALID FOR ALL OF THE TEZOS NETWORKS, WE STRONGLY SUGGEST YOU BEGIN ON ONE OF THE TESTNETS WHEN STARTING OUT INTERACTING WITH THE BLOCKCHAIN.

The ``tezos-client`` can be utilized to generate a cryptographic identity and to activate a basic wallet housed on the Tezos Blockchain.  Before you begin, you will need to acquire some free XTZs.  (These are testnet XTZs, currently there are no faucets for Mainnet XTZs.  Those 


In order that our development community have the tools to test and build upon the networks, we have a faucet that will provide you a JSON file with a balance of XTZs `faucet <https://faucet.tzalpha.net>`__. Please preserve the JSON file, since after each reset of Zeronet or Alphanet you will have to reactivate the wallet. Please drink carefully and don't abuse the faucet: it only contains 30.000 wallets for a total amount of ꜩ760.000.000.

Download the JSON file and save this in your ``$HOME/tezos`` directory.

The wallet from the faucet is in the form of a JSON file ``tz1__xxxxxxxxx__.json``

You can activate this wallet with the following command: 

::

    tezos-client activate account chorizorosebloom with "tz1__xxxxxxxxx__.json"
    
After running the ``activate account`` command you will notice that the ``$HOME/.tezos-client`` has been populated with 3 files:

``public_key_hash``, ``public_keys`` and ``secret_keys``

The content of each file is in JSON format and maintains the mapping between aliases (``chorizorosebloom`` in this case) and the expected name of the file. Secret keys are stored on disk encrypted with a password, unless using a hardware wallet (see :ref:`ledger`). An additional file ``contracts`` contains the addresses of ``originated contracts`` which are of the form *KT1…*.  Please note there are two types of addresses on the Tezos Blockchain ``"tz[1,2,3]_xxx_"`` addresses and "``KT1_xxx_``".  Originated accounts utilize the "KT1" format and implicit accounts are of the "tz1" type.  We will cover the differences in these two types of addresses and their functions in a later section. 

Now that we have an active wallet with loaded with some XTZ, we can check the balance of the new account with the following:

::

    tezos-client get balance for chorizorosebloom



Generating New Cryptographic Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generating new pairs of keys is  a core functionality of the ``tezos-client``.  


We can for example generate a new pair of keys, which can used locally with the alias *decet*:

::

      $ tezos-client gen keys decet
      
The newly generated keys will be stored in the JSON file located in ``$HOME/.tezos-client/public_keys`` under the name of the new alias (In this case *decet*) Be sure to make a back-up of this directory and that the password protecting your secret keys is properly managed.  

Tezos support three different public encryption schemes: *Ed25519*, *secp256k1* (Used in Bitcoin), and *P-256* (also called *secp256r1*). The two latter curves have been added for interoperability with Bitcoin and Hardware Security Modules (*HSMs*). Unless your use case requires those, you should probably use *Ed25519*. We use a verified library for Ed25519, and it is generally recommended over other curves by the cryptographic community, for performance as well as security reasons.



For more advanced key management we offer :ref:`ledger support <ledger>` and a :ref:`remote signer<signer>`.




Transactions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Let's transfer some XTZs to the newly created account:

::

   ./tezos-client transfer 500 from chorizorosebloom to decet --fee 0.05

The ``transfer`` command returns a receipt with all the details of the transaction, including its hash and then waits for the operation to be included in one block. 


::
    

      Warning:
  
                 This is NOT the Tezos Mainnet.
  
      The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

      Node is bootstrapped, ready for injecting operations.
      Estimated gas: 10100 units (will add 100 for safety)
      Estimated storage: no bytes added
      Operation successfully injected in the node.
      Operation hash: oob57Xce6RCBhKyCaAomr63weZDE4Q42nx7px7AN41F6yNT4bXT
      Waiting for the operation to be included...
      Operation found in block: BMCqpcUHzBoNoAh3gfqrRyVntY3k1A8PKv9SPo5qUn449eQeEaA (pass: 3, offset: 0)
      This sequence of operations was run:
      Manager signed operations:
      From: tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo
      Fee to the baker: ꜩ0
      Expected counter: 30597
      Gas limit: 10000
      Storage limit: 0 bytes
      Revelation of manager public key:
         Contract: tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo
         Key: edpkuxi3m5FawRV846Kjxd6mc46a6uU63yr2BALhqKBiN2BqJLeKXP
         This revelation was successfully applied
         Consumed gas: 10000
      Manager signed operations:
         From: tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo
         Fee to the baker: ꜩ0.05
         Expected counter: 30598
         Gas limit: 10200
         Storage limit: 0 bytes
         Balance updates:
            tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo ............ -ꜩ0.05
            fees(tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5,98) ... +ꜩ0.05
         Transaction:
            Amount: ꜩ500
            From: tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo
            To: tz1WjpQL5MoKK3eeSiZKooU8B58GSGVeawAV
            This transaction was successfully applied
            Consumed gas: 10100
            Balance updates:
            tz1aqSgSFwFSJT8t3GapbaBmbmYfvHRM6wvo ... -ꜩ500
            tz1WjpQL5MoKK3eeSiZKooU8B58GSGVeawAV ... +ꜩ500

            The operation has only been included 0 blocks ago.
            We recommend to wait more.
            Use command
              tezos-client wait for oob57Xce6RCBhKyCaAomr63weZDE4Q42nx7px7AN41F6yNT4bXT to be included --60  
              confirmations and/or an external block explorer.



If you want to simulate a transaction without actually sending it to the network you can use the ``--dry-run`` option.


As in any blockchain it is advisable to wait through the creation of several blocks before  considering the transaction as final.  We strongly reccomend that for any important operation you wait at least 60 blocks. We can do that with:

::

   tezos-client wait for <operation hash> to be included

In the rare instance that an operation is lost, how can we be sure that the operation will not be included in any future block and broadcast to the network? After 60 blocks an operation is considered invalid and cannot be included in any subsequent blocks. Furthermore, each operation has a counter (explained in more detail later) that prevents replays, so it is usually safe to re-inject an operation that has been lost after 60 blocks. 


Verifying Your Operation Or Transaction Via The Receipt 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After an operation, the client prints a ``receipt`` that recapitulates the effects of the operation on the blockchain for recall and use in later operations or transaction monitoring. It is possible to review the receipt of a transaction with the following call:

::

   ./tezos-client rpc get /chains/main/blocks/head/operations

A manager operation, such as a transaction, has 3 important parameters: counter, gas and storage limit. The counter belongs to each account, it increases at each operation signed by that account and enforces some good intuitive properties:

    - Each operation is unique: for example, if we perform twice the same transfer from *echopepper* to *decet*, even if all the data are the same, the   operation counter will be different thus assuring the identity of the individual operation.


    - Each operation is applied once: for example, if the two peer nodes receive the information that echopepper has transferred 500 XTZ to decet and     they both broadcast this transaction to another node, the operation will only be counted once by the receiving node. 

    - Maintains system in which operations are applied in order.

    - Ensurses all previous operations have been applied: if we broadcast operation *n* and *n+1*, and *n* is lost, then *n+1* cannot be applied.


Additionally, each operation must declare both a gas and storage limit: if an operation consumes more than the command line declared limits, it will fail. The storage and gas model will be discussed in future sections. 

Another interesting subset of the receipts are the ``balance updates``.  These show which accounts have been credited or debited. For the preceeding transaction, the updates are symmetrical: *echopepper* is debited 500ꜩ and *decet* is credited the same amount. The same is true for the fees, with the difference being that the baker is credited, but more importantly, the baker is not credited immediately on its primary account.  These XTZ are ``frozen`` for a predetermined numbers of cycles.  Frozen accounts will be discussed in more detail later in the Proof of Stake section.

It is also possible to review the receipt of the whole block:

::

   ./tezos-client rpc get /chains/main/blocks/head/metadata

Here we always see the deposit that the baker had to put down to bake the block, which is again a debit on its main account paired with a credit on its ``deposits`` account, and the creation of a reward, which is a single credit to the bakers ``rewards`` account.

An interesting block receipt is the one produced at the end of a cycle as many delegates receive a return of part of their unfrozen accounts.


.. _originated_and_implicit_accounts:

Originated And Implicit Accounts (*KT1* vs. *tz1*) And Smart Contract Beginnings 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


In Tezos there are two kinds of accounts: *implicit* (*tz1*) and *originated* (*KT1*).  Implicit accounts are the *tz1* addresses we have been using for illustrative purposes through this documentation.   Creation of a *tz1* account is as simple as generating a new set of keys and transfering XTZ to the associated public key hash (which is the *tz1_xxxx_* address created with the keys).

After running the ``gen keys`` command, you can locate the public key hash of your *tz1* (implicit) account by running the following command: 

::

    tezos-client list known addresses
    


Your address will be listed associated with the alias used to gnerate the keys.  In this example we have generated a set of keys for ``decet``

::

    Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

 decet: tz1WjpQL5MoKK3eeSiZKooU8B58GSGVeawAV (unencrypted sk known)


Originated accounts  (*KT1*) and are an extremely important component to the Tezos blockchain, as they can contain Michelson code, which is the smart contract language of the Tezos Protocol.  In order to delegate your coins to a bakery/delegate you must create an originiated account (*KT1*) from which to delegate your coins. We will cover this in detail during the Staking and Baking section.   As is convention in blockchain protocols, those blockchains with a programmable logic are considered powered by Smart Contracts.

After activating a wallet and funding with the faucet XTZ (JSON file),  lets originate account from which to delegate our XTZ.

::

   ./tezos-client originate account decetdelegator for decet transferring 100 fromtz1WjpQL5MoKK3eeSiZKooU8B58GSGVeawAV     
     
We have just originated an acccount ``decetdelegator`` and transfered 100 XTZ from our *tz1* address to our new KT1 address associated with the alias ``decetdelegator``.  

The command:

::

    ./tezos-client list known contracts 
    
    
will list both the *KT1* and *tz1* accounts associated with your tezos-client on your machine. The KT1 address will be listed after call and associated with the alias chosen in the origination operation.   

If decet wished to bake and receive delegations, a different sequence of commands and order of operations would be utilized.  A detailed explanation of this process will be covered in later sections.  

An understanding of how to deploy Michelson smart contracts is a vital component to anything other than trivial software development on the Tezos Blockchain.  Michelson will be covered in its own section with specifc tutorials in this documentaiton.  There are also a few different higher level langagues in development that compile to  Michelson ( Liquditiy is the furthest along in development).  There are other resources and tutorials on Smart Contract composition for the Tezos Network, to be listed in addenda to this documentation.  

As OCAML is the development language for Tezos, and understanding the key differences between functional and imperative programming will be very beneficial when developing with OCAML.  There are many tutorials avaialable to acquaint oneself with Ocaml and functional programming.  Michelson was developed with Ocaml in addition to rest of the core Tezos protocols.   




Storage and Gas Cost Model 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Storage Cost Model encourages leanness and efficiency in smart contracts and programs.  As a contract saves its data at a *KT1* (originated) account address on the blockchain and every node stores the entirety of the blockchain in the ``$HOME/.tezos-node directory, charging a fee per byte for data stored on the blockchain discourages abuse and serves to maintain transaction integrity.  

As larger programs will necessarily take up more  allocated data space, the cost model ensures that large development projects do not unnecessarly burden the network with bloated or clunky programs. 

We can call a program that takes a string as an argument and pass the ``--dry-run`` option to compare the storage costs of string of differing lengths. The transaction will update the storage if the string length (and therefore the same number of bytes) is identical.  First, lets call a program and pass the string '"funky"' as its initial argument.

Let's see what calling a program with a new argument would look like with the ``--dry-run`` option:

::

   tezos-client transfer 0 from decet to stitchfordvincentino --arg '"funky"' --dry-run

The transaction would successfully update the storage but this time it wouldn't cost us anything more than the fee, the reason being that the storage for ``"world"`` is the same as for ``"hello"``, which has already been paid for. To store more we'll need to pay more--you can try by passing a longer string.

The other cost associated with running contracts is the *gas*, which measures *how long* does a program take to compute. Contrary to storage there is no cost per gas unit, a transfer can
require as much gas as it wants, however a baker who has to choose between several transactions is much more likely to include a low gas one because it's cheaper to run and validate. At the same time, bakers also give priority to high fee transactions. This means that there is an implicit cost for gas that is related to the fee offered versus the gas and fees of other transactions.

If you are happy with the gas and storage of your transaction, you can run it for real, however it is always a good idea to set explicit limits for both. The transaction fails if the limits are passed.

::

   tezos-client transfer 0 from decet to stitchfordvincentino --arg '"funky"' \
                                            --gas-limit 1475 \
                                            --storage-limit 46

A baker is more likely to include an operation with lower gas and storage limits because it takes fewer resources to execute: it is in the best interest of the user to pick limits that are as close as possible to the actual use.

More test contracts can be found in directory :src:`src/bin_client/test/contracts/`. An advanced documentation of the smart contract language is available :ref:`here<michelson>`. For additional details and examples, see https://www.michelson-lang.com/.


Validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Tezos Node has the capability to validate an operation before broadcasting it to the network.   The node simulates the operation given the current context of the blockchain and then bases its validation on the results.

In general,if an invalid operation is transmitted, for example sending more XTZ than you have in your wallet, the node will broadcast it, and when the invalid operation is included in a block you'll have to pay the usual fee, even if it won't have an effect on the context. To avoid this case, the Tezos Client utilizes the node's capability to simulate as a tool for validation. 

The same validation process is used when you pass the ``--dry-run`` option:the receipt that you see is actually a simulated one.

Another important use of validation is to determine the gas and storage limits. The node first simulates the execution of a Michelson program taking miniscule amount of gas and storage. Based on the results of this simulation by the node,  the client sends the transaction with the correct amounts of gas and storage. This funcitonality allows the client to send transactions without specifying the limits. The gas and storage where computed based on the data from the node simulation automatically. 

More information on validation can be found at :ref:`here. <validation>`

It's RPCs All The Way Down 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Tezos client communicates with the Tezos node uniquely through RPC calls, so make sure that the node is listening and that the ports are correct.  (``--rpc-addr 127.0.0.1:8732``) To illustrate this, please recall the the ``get timestamp`` command mentioned above, that command is shortened from the following RPC:

::

   tezos-client rpc get /chains/main/blocks/head/header/shell

The client tries to simplify interactions with the Tezos network as much as possible. However if you want to query the node for more specific informations you'll have to use RPCs.

 For example, to check the value of important constants in Tezos, which may differ between Betanet, Alphanet and Zeronet, you can use:

::

   tezos-client rpc get /chains/main/blocks/head/context/constants

   gs@decet:~/tezos$ ./tezos-client rpc get /chains/main/blocks/head/context/constants
Warning:
  
                 This is NOT the Tezos Mainnet.
  
     The node you are connecting to claims to be running on the
               Tezos Alphanet DEVELOPMENT NETWORK.
          Do NOT use your fundraiser keys on this network.
          Alphanet is a testing network, with free tokens.

{ "proof_of_work_nonce_size": 8, "nonce_length": 32,
  "max_revelations_per_block": 32, "max_operation_data_length": 16384,
  "max_proposals_per_delegate": 20, "preserved_cycles": 3,
  "blocks_per_cycle": 2048, "blocks_per_commitment": 32,
  "blocks_per_roll_snapshot": 256, "blocks_per_voting_period": 8192,
  "time_between_blocks": [ "30", "40" ], "endorsers_per_block": 32,
  "hard_gas_limit_per_operation": "400000",
  "hard_gas_limit_per_block": "4000000",
  "proof_of_work_threshold": "70368744177663",
  "tokens_per_roll": "10000000000", "michelson_maximum_type_size": 1000,
  "seed_nonce_revelation_tip": "125000", "origination_size": 257,
  "block_security_deposit": "512000000",
  "endorsement_security_deposit": "64000000", "block_reward": "16000000",
  "endorsement_reward": "2000000", "cost_per_byte": "1000",
  "hard_storage_limit_per_operation": "60000" }
 


   }

You can find more info in the :ref:`RPCs' page. <rpc>`
