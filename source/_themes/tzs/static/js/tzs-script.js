(function ($) {

	$(document).ready(function () {

		$('.tzs-sidenav li').each(function () {
			if ($(this).find('ul').length > 0) {
				//$(this).prepend("<span class='open-sidenav'></span>");
				var A = $(this).children('a').first();
				if ($(this).hasClass('current')) {
					A.html(A.html() + '<i class="fa fa-chevron-down"></i>');
				}
				else {
					A.html(A.html() + '<i class="fa fa-chevron-down close"></i>');
				}

				$(this).addClass('has-sub-nav');
				$(this).find('ul').addClass('sub-side-nav');
			}
			if ($(this).hasClass('current')) {
				$(this).children('ul').css('display', 'block');
			}
		});
		/*
	    $('.tzs-sidenav .open-sidenav').on('click',function(){
	      $(this).parent('li').toggleClass('current');
	      $(this).siblings('.sub-side-nav').slideToggle(100);
	      return false;
	    });
		*/

		//when I click on an item on the TOC that has a dropdown, instead of going to the table of contents page, it should open the dropdown.
		$('.tzs-sidenav a').on('click', function () {


			var sub_menu = $(this).next('.sub-side-nav');
			if (sub_menu.length) {
				$(this).parent('li').toggleClass('current');
				$(this).siblings('.sub-side-nav').slideToggle(100);
				var icon = $(this).children('i');
				if (icon.hasClass('close')) {
					icon.removeClass('close');
					return true;
				} else {
					icon.addClass('close');
				}
				return false;

			}

			if ($(this).attr('href') == "/") {
				return false;
			}

			return true;

		});

		$("#tzs-hamburger-menu-icon").on('click', function () {
			$("#sidebar2").toggleClass('expand');

		});
		handleSidebar2Mobile();


	});//end document ready

	$(window).resize(function () {
		handleSidebar2Mobile();
	});

	function handleSidebar2Mobile() {
		console.log("handleSidebar2Mobile");
		var ww = $(window).width();
		var wh = $(window).height();
		$("#sidebar2").css('max-height', (wh - 60) + 'px').css('overflow', 'scroll');

		if (ww < 768) {
			$("#tzs_content_col").removeClass('col-sm-8').addClass('col-sm-7');
			$("#tzs_sidebar2_col").removeClass('col-sm-4 tzs_sidebar2_col_as_last').addClass('col-sm-3');
			$("#sidebar2").addClass('sidebar2-mobile');
			var elm = $("#tzs-content-inner");
			var rt = $(document).width() - (elm.offset().left + elm.outerWidth());
			$("#sidebar2").css({ top: 60, right: rt });

			//move search box to avbove the navigation
			$("#tzs_search_box").insertBefore("#tzs_sidenav");
			$("#sidebar").append($("#tzs_side_box_1"));
			$("#sidebar").append($("#tzs_side_box_2"));

		}
		else {
			$("#sidebar2").removeClass('sidebar2-mobile');
			$("#sidebar2").css({ top: 'auto', right: 'auto' });
		}
		if (ww > 767 && ww < 992) {
			$("#tzs_content_col").removeClass('col-sm-7').addClass('col-sm-8');
			$("#tzs_sidebar2_col").removeClass('col-sm-3').addClass('col-sm-4 tzs_sidebar2_col_as_last');
			$("#tzs_sidebar_col").addClass('hidden');

			//$("#tzs_search_box").insertBefore("#tzs_sidenav");
			$("#sidebar2").append($("#tzs_search_box"));
			$("#sidebar2").append($("#tzs_side_box_1"));
			$("#sidebar2").append($("#tzs_side_box_2"));
		}
		else if (ww > 991) {
			$("#tzs_sidebar_col").removeClass('hidden');
			$("#tzs_content_col").removeClass('col-sm-8').addClass('col-sm-7');
			$("#tzs_sidebar2_col").removeClass('col-sm-4 tzs_sidebar2_col_as_last').addClass('col-sm-3');
			$("#sidebar").append($("#tzs_search_box"));
			$("#sidebar").append($("#tzs_side_box_1"));
			$("#sidebar").append($("#tzs_side_box_2"));
		}

	}

})(jQuery);
