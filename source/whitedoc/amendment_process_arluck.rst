.. _amendment-process-arluck:

--------------------------------


Amending Tezos -- Jacob Arluck
=================================

Traversing The Amendment Process

Jacob Arluck
Nov 29, 2018
https://medium.com/tezos/amending-tezos-b77949d97e1e
Reprinted With Permission From Author

Background
--------------------------------

Tezos is a self-amending blockchain network which incorporates a formal, 
on-chain mechanism for proposing, selecting, testing, and activating protocol
upgrades without the need to hard fork.

In this post, I describe how the Tezos amendment process works in practice and 
propose several ways to improve it in the near-term. Deeper questions, like 
splitting voting rights from consensus, or how amendments should be funded 
on-chain are outside the scope of this post, but important topics for future 
consideration.

This piece assumes a basic knowledge of Tezos baking. I recommend reading 
“It’s a baker’s life for me” and the Tezos proof-of-stake documentation to 
get started.

* I use the expression “amendment process” instead of “election cycle” to 
avoid confusion with “cycles” of baking.

Understanding the Tezos amendment process
The amendment process can be broken into four discrete periods: 
the Proposal Period, the Exploration or “Testing” Vote Period, the 
Testing Period, and the Promotion Vote Period. Each of these four periods lasts 
eight baking cycles (i.e. 32,768 blocks or roughly 22 days, 18 hours), 
comprising almost exactly three months from proposal to activation.

As summarized in the flowchart diagram below, any failure to proceed to the
subsequent period reverts the network back to a Proposal Period. In other words,
failure to proceed restarts the entire amendment process.

This is precisely what has been happening since the Tezos genesis block was a
dopted by the community in June. In the absence of any proposals, 
a new proposal period has restarted every eight cycles since Block 1.

So how does it work?

Proposal Period
--------------------------------

The Tezos amendment process begins with the Proposal Period, during which 
bakers can submit proposals on-chain via the proposal operation, which takes 
“source”, “period” and “proposals hash” as parameters. The “source” is the baker
who submits the proposal operation, the “period” specifies the specific proposal 
period in which the proposal was submitted, and the “proposals hash” is the hash 
of the tarball of concatenated .ml/.mli source files.

Bakers may submit up to 20 proposals in each Proposal Period. When submitting
a proposal, the baker is also submitting a vote for that proposal, equivalent 
to the number of rolls in its staking balance at the start of the period.

For those wanting to follow along, Tzscan.io (and hopefully other Tezos block 
explorers) allows you to watch incoming proposals.

Other bakers can then vote on proposals by submitting proposal operations of 
their own. As described in the white paper, the Proposal Period vote is done 
via approval voting, meaning each baker may vote once on up to 20 proposals. 
Think of it as a form of “upvoting.”

At the end of the Proposal Period, the network counts the proposal votes and 
the most-upvoted proposal proceeds to the Exploration Vote Period. If no 
proposals have been submitted or if there is a tie between proposals, a new 
Proposal Period begins.

 Exploration Vote Period (known as “Testing Vote” in the current protocol)
 --------------------------------

In the Exploration Vote Period, bakers may vote on the top-ranked proposal 
from the previous Proposal Period.

Voting is done on-chain via the Ballot Operation, which takes 
“source,” “period_n,” “proposal,” and “ballot.” The “source” is the baker,
“period_n” is the specific voting period in which the operation was submitted, 
“proposal” specifies the proposal, and “ballot” specifies the baker’s vote, 
either “Yay,” “Nay,” and “Abstain.”

As in the Proposal Period, a baker’s vote is based on the number of rolls
in its staking balance at the beginning of the Exploration Vote Period. Each 
baker may send a ballot operation only once during the voting period.

At the end of eight cycles, the network counts the votes. If voting 
articipation (the total of “Yays,” “Nays,” and “Abstains”) meets the quorum
(explained below) and an 80% supermajority of non-abstaining bakers approves, 
the proposal proceeds to the Testing Period.

If the quorum or the 80% supermajority are not met, the amendment process
reverts to the beginning of the Proposal Period.

A notable difference from the Proposal Period is that after the Exploration 
Vote Period, the quorum is updated as follows where Q_t is the previous quorum 
and q_t is the total participation:


This adjusts the quorum to match an exponential moving average of 
the past participation rate.

In the genesis block, the quorum began at 80% and will remain at 80% until 
the end of the first Exploration Vote Period.

 Testing Period
 --------------------------------

If a proposal is approved by a supermajority in the Exploration Vote Period, 
the Testing Period (8 cycles) begins with a testnet fork that runs parallel to 
the main Tezos chain for 48 hours before terminating. This 48-hour constant has 
been set conservatively in order to reduce the risk of the network perceiving t
he testnet fork as the main chain.

Just as described in the 2014 white paper, proposals have access to a small 
standard library, but are sandboxed and can not make any system calls.

The purpose of the Testing Period is to evaluate whether a proposal is a worthy 
amendment to the protocol.

The testnet fork ensures the upgrade does not corrupt the context and that, 
should the upgrade be adopted, the network would continue making valid state 
transitions. However, 48 hours of testing by itself is decidedly too short to 
determine whether a proposal would be a worthwhile and safe amendment. In 
particular, it can maintain liveness longer than 48 hours and does not contain 
security vulnerabilities). As a result, a testnet matching the amendment 
proposal is likely to run off-chain for the remaining ~7.3 cycles of the 
Testing Period, allowing stakeholders to evaluate and discuss the amendment as
gain better knowledge of its properties.

 Promotion Vote Period
--------------------------------

At the end of the Testing Period, the Promotion Vote Period begins.
In this period, the network decides whether to adopt the amendment based
on off-chain discussions and its behavior during the Testing Period.

As in the Exploration Vote Period, bakers submit their votes using the ballot 
operation, with their votes weighted proportionally to the number of rolls in 
their staking balance at the beginning of the Promotion Vote Period. As in the 
Exploration Vote Period, each baker may send only one ballot operation during 
this period.

At the end of the Promotion Vote Period, the network counts the number of votes.
If the participation rate reaches the minimum quorum and an 80% supermajority of 
non-abstaining bakers votes “Yay,” then the proposal is activated as the new 
mainnet.

Otherwise, the process once more reverts back to the Proposal Period. The 
minimum quorum is updated based on the participation rate using the same 
formula which was used at the end of the Exploration Vote Period:


Once the amended protocol is activated, a new Proposal Period begins and the 
amendment process begins anew.


Improving the Tezos Amendment Process
--------------------------------

As it stands, the current Tezos amendment process is designed with a simple,
but effective mechanism for amending both the protocol and the amendment process 
itself.

The current design is well-suited to bootstrapping improvements 
to Tezos in its early days, but below are five glaring opportunities 
for improvement in the current on-chain implementation.

There are also opportunities to encourage healthy coordination off-chain. 
Two examples that come to mind are incentivized testnets 
(a la Cosmos’ Game of Stakes) and a discussion forum based on reputation/stake),
but these fall outside the scope of this piece.

Lengthen the proposal period and introduce proposal fees
Lengthening the proposal period to 12 or 16 cycles would allow more time to
debate and coordinate around proposals. Furthermore, a requirement that 
proposals be submitted within the first 8 cycles would ensure sufficient 
time for discussion before the amendment proceeds to an Exploration Vote Period.
This requirement would also prevent proposers from sniping the end of a Proposal
Period in which no proposals or only weakly upvoted proposals 
had been submitted.

Proposal quality also represents a modest risk in the current Tezos network,
particularly if some bakers face a coordination cost in distinguishing 
legitimate proposals from malicious or mediocre ones. Ultimately, it should be
inexpensive to vote on proposals, but submitting one should incur a cost to 
deter spam and frivolous proposals.

Introducing an upfront proposal fee of say, 500 xtz seems like a simple yet 
reasonable precaution that reduces the risk of proposal spam in the early days
of the network.

Alternatively, a time-locked deposit could be priced at a higher level (
e.g. 2,500 xtz) and returned to the proposer if the proposal is upvoted by
at least 10% of the voting weight in the Proposal Period.

 Evaluate voting weight at the end of voting periods, rather than the 
beginning
--------------------------------

Because bakers’ staking balances are counted at the start of each voting period,
delegators cannot change their delegation once voting has begun. 
This forces delegators to trust delegates to commit truthfully to the votes 
they signaled before the start of the period.

Evaluating a voter’s staking balance at the end as opposed to the beginning of 
the voting period would allow delegators to move their stake once they’ve seen 
how their delegate voted. At least for now, this also encourages delegates to 
create one delegation address for those preferring “Yay” for a given proposal 
and another for “Nay.” This would allow delegators to keep their 
preferred delegate(s) but still hold influence in the amendment process.

At first this may seem counterintuitive, because delegators are used to 
waiting 7 cycles before their delegated tokens actually provide baking 
rights. In reality, rolls are counted in real time when delegated, 
ven though they affect baking rights only after 7 cycles.

That said, switching also incurs other meaningful transaction costs. 
There are search costs associated with finding a new delegate who votes 
as desired, but who does not tradeoff payout structure or reliability. 
Another concern is that a delegate could try to retaliate against delegators
who switch during a voting period. One possible solution would be to enable 
delegators to secretly override their delegate’s vote.

3. Longer testnet forks and a longer testing period overall

Lengthening the overall Testing Period to 12 cycles from 8 would provide 
stakeholders more time and data by which to evaluate and discuss the properties of a given proposal.

That said a 2-day testnet fork may not provide a long enough period 
to evaluate the persistence of the amended protocol. Conducting the testnet 
fork for one week could incrementally improve the realism of the on-chain test fork.

Proposals will have been tested in off-chain environments 
long before the Testing Period and the Testing Period may evolve into more 
of an “Evaluation period”.

Another possibility would be to eventually create a testnet that’s permissioned
on-chain via a DAO or elected council. The decision-making latitude of this 
council could be narrowly specified: the council’s capability could be limited 
to delaying advancement to the Promotion Vote Period by a fixed number of cycles for the express purpose of allowing more time for further evaluation.
By contrast, a more aggressive approach would entail the council voting to 
advance the proposal to a Promotion Vote Period at predefined intervals 
(e.g. at 8 cycles or 16 cycles).

 Quorum floor
 --------------------------------

The Tezos quorum adjustment mechanism is a simple, yet powerful tool to avoid
stagnation as a result of low stakeholder participation and lost coins. 
However, many successive Voting Periods (e.g. 1 year) with low participation 
could drive the quorum to low levels, allowing proposals to pass without much
support. Although malfeasance can be blocked by mobilizing “nay” votes,
setting a minimum quorum may be worthwhile as an additional protection.

One possibility would be for the supermajority approval threshold in the 
Voting Periods to adjust based on the participation rate. Under this model, 
the network would require an 80% supermajority to vote “Yay” at a 60%
participation rate and a 90% supermajority at a 50% participation rate.

Alternatively, it’s also possible to require the quorum to be greater than the 
average participation rate. For instance:


Following this formula, if the average participation rate were 0%, the network
would maintain a floor of 20%. If the average participation rate were 50%, 
then the minimum quorum would be 60%, and if the average participation rate 
 were 80%, then the minimum quorum would be 84%.
 
Delay activation of the amended protocol
--------------------------------

The current Tezos protocol activates amendments directly at the end of the 
Promotion Vote period. However, it would be prudent to delay activation by a 
predefined number of baking cycles to ensure that the network is prepared for 
the new protocol (particularly if the protocol incorporates changes to the 
consensus itself). This could also, conceivably, delay the start of the next 
Proposal Period as a buffer period to allow bakers to adjust to the workings 
of a new protocol.

Amendment RPCs
--------------------------------

This week’s Proto3 update has paved the way by adding a host of RPCs to support 
the amendment process. This will allow the Tezos community to interact with 
proposals, check the quorum, see the number of ballots cast, and refer to other 
important amendment-related data mentioned throughout this piece.

Conclusion
--------------------------------

As detailed, Tezos implements a multi-stage amendment process that allows 
stakeholders to formally propose, select, test, and activate upgrades to the
protocol on-chain.

But we should remember, first and foremost, that blockchain governance remains
a nascent field. There is still little in the way of directly analogous 
experience to draw upon.

This demands that we wade in slowly and take great caution. Incentivizing 
proposal quality, encouraging greater delegate accountability, and providing
a longer testing period seem reasonable first steps to improve the amendment 
process. But these are merely places to start.

What is most important is that we learn from our early experiences with the 
amendment process. And even with early success, we should be wary of 
the existing system’s limitations, particularly its reliance on voting, 
over the longer term. It is critical to explore complementary mechanisms: 
especially, futarchy, programmatic constitutionalism, and on-chain councils.

After all, blockchain governance should not be merely about the “next” upgrade, 
but rather about creating internet-native institutions that are built to last,
ones that can be adopted sustainably by hundreds of millions, if
not billions of users some day. Introducing the Tezos amendment process aims to
be a step toward that goal. 
