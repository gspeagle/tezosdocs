.. _arluck-liquid-proof-of-stake:

--------------------------------

Liquid Proof-of-Stake - Jacob Arluck
===================================

Jacob Arluck

Jul 30, 2018


tl;dr: Tezos allows token holders to transfer (“delegate”) validation rights to other token holders without transferring ownership. This has led many observers to confuse Tezos’ consensus mechanism with the delegated proof-of-stake (“dPOS”) model specific to EOS or Lisk. In dPOS, election of a fixed set of Block Producers (i.e. delegates) is required for network consensus. In Tezos, delegation is optional. I propose we instead refer to Tezos’ consensus mechanism as “Liquid Proof-of-Stake” or “LPOS”. LPOS aims to maintain a dynamic validator set, facilitating token holder coordination and accountable governance.


All in all, the barrier to entry to validation in Tezos is significantly lower than in standard dPOS (or proof-of-work for that matter), with the current protocol requiring a baker to have 10,000 ꜩ, a reliable internet connection, good opsec, modest computing power and technical know-how.
Baking in Tezos
Whereas in Bitcoin a miner’s right to publish a block is determined by solving a cryptographic puzzle, a mechanism known as proof-of-work, Tezos allocates block publishing rights based on stake, a mechanism known as proof-of-stake. Each block is produced (“baked”) by a random stakeholder and notarized (“endorsed”) by 32 other random stakeholders.

Like Bitcoin, Tezos uses inflationary block rewards and transaction fees to incentivize validators (or “bakers” in Tezos parlance) to participate in consensus. To incentivize honest behavior around the time of block creation, Tezos requires a baker to put up a safety deposit for several weeks. If a baker explicitly tries to double bake or double sign blocks, it forfeits this safety deposit.

Token holders not interested in baking themselves may delegate baking rights without transferring token ownership. As described earlier this year in “It’s a baker’s life for me”:

Since not everyone holding tokens is interested in being a baker, tokens can be “delegated” to another party. The delegate does not own or control the tokens in any way. In particular, it cannot spend them. However, if and when one of these tokens is randomly selected to bake a block, that right will belong to the delegate.
But unlike in Bitcoin, in which miners capture all inflationary block rewards, Tezos delegates compete by sharing their inflationary baking rewards with delegators. All Tezos token holders, regardless of stake, can avoid being diluted by the targeted ~5.5% annual inflation, fostering greater coordination.

Delegated Proof-of-Stake (dPOS)
Because Tezos allows delegation, its consensus mechanism has often been described as delegated proof-of-stake, or “dPOS”. This has caused many observers to confuse proof-of-stake in Tezos with a very specific consensus mechanism used by EOS, BitShares, TRON, ARK, and Lisk*.

But in the standard dPOS model, delegation to a fixed number of block producers or “witnesses” (i.e. 21 Block Producers in EOS) is a requirement for network consensus rather than an option as in Tezos.

The use of dPOS for consensus typically stems from the belief that scalability is the main bottleneck to mainstream adoption of blockchain-based dapps. Or in the words of EOS documentation:

The EOS.IO software introduces a new blockchain architecture designed to enable vertical and horizontal scaling of decentralized applications. […] The resulting technology is a blockchain architecture that scales to millions of transactions per second, eliminates user fees, and allows for quick and easy deployment of decentralized applications.
EOS takes dPOS further by adding Byzantine Fault Tolerance, by which dPOS-inventor Dan Larimer claims the network can achieve 99.999% finality in less than a second.

In other words, dPOS might finally allow us to leave behind an era in which a pack of digital kittens can congest the proverbial world computer with non-fungible fur balls.

To keep this small set of block producers honest and accountable, dPOS protocols continuously vote to select a set of delegates to publish and validate blocks. In EOS, token holders elect 21 Block Producers and 100 standby Block Producers, with results calculated every two minutes. In Lisk, token holders constantly elect 101 delegates. Voting power in dPOS protocols is typically proportionate to the token holder’s stake.

Larimer argues dPOS also offers greater decentralization compared to Bitcoin and Ethereum, in which mining activity has become quite centralized. Unlike Tezos, which assigns baking rights proportional to stake, dPOS splits block production rights evenly within the set of active block producers. The risk of Sibyls and cartels aside, the 21st most voted validator technically has as much say as the most popular block producer.

And with a fixed validator set, dPOS also requires less inflation to incentivize block production. Of its 5% annual inflation rate, EOS allocates only 1% to Block Producers (a share goes to standby block producers), with the other 4% allocated to an on-chain treasury intended to fund public goods for the network.

Fixing the validator set to 21 or 101 nodes harnesses intense competition between aspiring block producers, but also creates a barrier to entry in consensus participation. After all, EOS’ plans to scale to millions of transactions per second, likely requiring any block producer to meet significant infrastructure requirements. The entire EOS Block Producer list is likely to become professionalized to the scale of modern bitcoin mining (or even be Bitcoin miners themselves), a far cry from the dorm room GPU mining of the early Bitcoin days.

Towards Liquid Proof-of-Stake
But unlike in dPOS, delegation is not the salient aspect of proof-of-stake in Tezos. The project holds decentralization, coordination, and security as core priorities over scaling immediately and throughput (currently ~40 tps) is to be dialed up carefully.

In fact, Tezos’ flavor of proof-of-stake is more akin to “proof-of-stake with delegation” than it is to dPOS. To resolve the confusion and better describe proof-of-stake in Tezos, I propose we begin to call the Tezos consensus mechanism “Liquid Proof-of-Stake”, or “LPOS”.

In the weeks since betanet launch, Tezos has seen rapid growth in the number of delegates and 411 bakers are set to bake in cycle 15. But as the diagram below demonstrates, we have already seen some churn in the delegation market as delegates compete based on fees, payout frequency, reputation, and other metrics.

In Tezos, delegate misbehavior (e.g. not paying out rewards) is likely to be detected quickly and the community is quick to castigate a delegate for charging high fees. Token holders face little friction when switching delegates and thus can credibly threaten to delegate elsewhere, incentivizing coordination instead.


Above is the distribution of bakers from cycle 1 to 15 of Tezos betanet. Baking has decentralized significantly since launch and will decentralize further as better infrastructure becomes widely available.
The protocol will eventually require fewer tez to bake (e.g. 2,000 ꜩ instead of the current 10,000 tz) than the tez in the median wallet (2,332 ꜩ) created in the 2017 fundraiser. This offers a viable path to baking for many thousands of users.

And exciting infrastructure efforts, such as BakeChain, “set and forget” baking hardware (“easy-bake-oven”, anyone?), and perhaps someday mobile-controlled bakers, are all likely to reduce frictions to baking further still.

Implications for on-chain governance
All of this becomes especially important in the context of on-chain governance, because consensus participants are the decision-makers regarding protocol upgrades in Tezos. A token holder who delegates baking rights also delegates voting rights.

On this topic, there’s no shortage of celebrated blog posts forewarning us that on-chain governance is destined for capture by concentrated interests. And there is real evidence to this effect in Lisk. But observers too often extrapolate the experiences of dPOS protocols with fixed size validator sets directly onto Tezos. Rarely do observers note that Tezos features a different model based on different priorities.

None of this is to say that powerful voting coalitions can’t arise, or that large entities, like exchanges, won’t become important stakeholders in Tezos. But what matters most is whether token holders and users can coordinate to rearrange power and hold concentrated interests accountable. Liquid Proof-of-Stake should facilitate this by ensuring the barrier to entry is low enough to maintain a dynamic validator set.

But like infosec, accountable governance is a challenge which can never be “solved” absolutely.

Alternatively, baking and voting rights could be unbundled entirely, further loosening the connection between consensus and on-chain governance. This may require changing strong defaults, as users might delegate their baking rights to one entity and their voting rights to another. Not to mention all the other potential tradeoffs. But it is an intriguing area of research and, of course, in need of a subsequent Medium post.

Notes
[*] There are nuanced differences between different dPOS protocols, but they usually follow th