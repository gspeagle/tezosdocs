---------------------------------------------

Welcome To Decet's Curation Of The Tezos Developer Documentation 
================================================================


Generously Sponsored By A Grant From The Tezos Foundation. 
Development On The Tezos Protocol Is Dynamic And Continous;
All software development is recorded and housed on GitLab:

https://gitlab.com/tezos/tezos

=============================================


**EDITOR'S NOTE:** The following introduction and subsequent tutorials utilize jargon and terms that are specific to distributed ledger and blockchain technology. Although documentation for the uninitiated is in development, these instructions and “How To’s” currently require an understanding of UNIX-based commands and some familiarity with building and compiling software from sources via command line interfaces.

If you are new to cryptocurrency and blockchains, or are perplexed by the term “decentralized,” please refer to our `FAQ <https://decet.io/faq/>`_ to acquaint   one's self with Tezos and the ecosystem from which it sprang.



The Project
-----------

Tezos is a blockchain that uses protocols and technologies similar to the network consensus capabilities of Bitcoin and Ethereum. Like those consensus mechanisms, Tezos' is vital to ensuring the validity of the transactions and the validity of the blockchain itself.  In a novel application of consensus mechanism. Tezos applies those same principles to our own protocol and network of nodes supporting the protocol. Importantly, this new and experimental step in governance is our method for  arriving at a consensus on the direction of the protocol itself--this is essentially a new addition to the typical cannon of administrative and governing philosophies.


Information on the Tezos Protocol and Tezos Foundatoin can be found at the following links (respectively):
https://tezos.com  & https://tezos.foundation 




The source code of Tezos is placed under the MIT Open Source License.


The Network(s)
---------------------

.. _mainnet:

Mainnet
~~~~~~~~~~~~

The Tezos network was been open and live since June 30, 2018. Transactions on the mainnet use  live XTZ currency that was allocated to donors in the genesis block, as well as the XTZ distributed as a reward to those baking on the network.

Over the  Tezos e of this documentation, we will take the user through the process of running a Tezos network node, creating a “wallet”, activating a donor account, and “staking and baking” your XTZs. The staking and baking process is the way in which the Tezos protocol both incentivizes participation and rewards bakers. This is done via two types of processes, both of which are vital to the protocol’s proof of stake consensus algorithm.

[Refer Here for activating a donor account]    <www.linktoHowToActivate in Documentation>


Testnets
~~~~~~~~~~~~

.. _alphanet:

- **The Tezos Alphanet** is a test network for the Tezos Blockchain.  Testing is vital to the success of any software project, and the Alphanet provides free XTZs so developers can perfect and debug their smart contracts before live deployment. It is highly recommended that any interaction with the Tezos protocol or blockchain begin on the Alphanet.

.. _zeronet:

- **The Zeronet** is a product and platform for the core development team. Due to the fast paced and often unpredictable nature of software development, the Zeronet is often restarted without notice several times a day. No support is offered for the Zeronet. We encourage all developers to begin protocol development with the Alphanet.


Getting started
---------------


To begin your explorations and investigations into the Tezos project, please begin with the following How To’s:

- HowToGet_

.. _HowToGet: introduction/howtoget.html

- HowToUse_

.. _HowToUse: introduction/howtouse.html

- HowToRun_

.. _HowToRun: introduction/howtorun.html

---------------------------------------

.. _support:


Technical Support
=================

If you need help understanding how the Tezos protocol works or if you
have technical questions about the software, here are a few resources
to find answers.

- `This documentation! <http://tezos.gitlab.io/>`_
  Make sure to go through this technical documentation before asking
  elsewhere, there is also a searchbox on the top left corner.
- Tezos `Stack Exchange <https://tezos.stackexchange.com>`_ is live
  (still in beta). If you don't find the answers you are looking for,
  feel free to ask questions!
- There is a sub-reddit at https://www.reddit.com/r/tezos/ that is the
  main meeting point for the Tezos community, for technical,
  economical and just random questions. They also have a nicely
  curated list of resources.
- For anything baking related, `Obsidian Systems
  <https://obsidian.systems>`_ is running a dedicated Slack channel,
  contact them to get access.
- There is a *#tezos* IRC channel 
Repositoryon *freenode* that is reserved for
  technical discussions and is always very active.
- There is a matrix channel *Tezos* that you can join `here <https://riot.im/app/#/room/#tezos:matrix.org>`_.

Community Links and Resources
-----------------------------

- Community Built Block Explorers: http://tzscan.io, https://tezex.info

- Tezos Riot Channel: https://riot.im/app/#/room/#tezos:matrix.org

- Tezos Sub-Reddit: https://www.reddit.com/r/tezos/

- Community FAQ: https://github.com/tezoscommunity/faq/wiki/Tezos-Technical-FAQ

- IRC Channel on *freenode* (reserved for developers and technical discussions): **#tezos**

- Slack Room For Bakers (Please email the following to request access): tezos@obsidian.systems



.. toctree::
   :maxdepth: 2
   :caption: Introduction:
   
   introduction/howtoget
   introduction/howtouse
   introduction/howtorun
   introduction/various
   introduction/contributing
   introduction/glossary

.. toctree::
   :maxdepth: 2
   :caption: White Docs:
   
   whitedoc/the_big_picture
   whitedoc/michelson
   whitedoc/proof_of_stake
   whitedoc/arluck_liquid_proof_of_stake  
   whitedoc/voting
   whitedoc/amendment_process_arluck
   whitedoc/p2p
   whitedoc/validation

.. toctree::
   :maxdepth: 2
   :caption: Protocol Updates & Amendments:
  
   protocols/Protocol_003_PsddFKi3 
   protocols/athens
   protocols/arluckathens

.. toctree::
   :maxdepth: 2
   :caption: Developer Tutorials:

   tutorials/rpc
   tutorials/data_encoding
   tutorials/error_monad
   tutorials/michelson_anti_patterns
   tutorials/entering_alpha
   tutorials/protocol_environment
   tutorials/profiling

.. toctree::
   :maxdepth: 2
   :caption: APIs:

   README
   api/api-inline
   api/cli-commands
   api/rpc
   api/errors

Indices and tables
==================

* :ref:`modindex`
* :ref:`search`
