.. _athens-04:

------------------------------------

Athens 
====================================

https://blog.nomadic-labs.com/athens-our-proposals-for-the-first-voted-amendment.html

NOMADIC LABS
Athens: Our Proposals for the First Voted Amendment
Mon 25 February 2019 in in-depth

This blog post is a preview of Athens: our protocol proposal for the first voted upgrade of Tezos. As announced in the last meanwhile at Nomadic, we shall propose two upgrades: one lowers the roll size to 8,000 tez, the other leaves it unchanged at 10,000 tez. Both alternatives will include an increase of the gas limit.

The hashes of both versions will be proposed on mainnet later this week, now that a new proposal period has begun.

Later this week, we will publish a blog post which provides the hashes and instructions for voting, as well as optional instructions for bakers and enthusiasts who want to participate in the tests.

Bakers who wish to upvote our proposals will have roughly three weeks to do so.

The same day we will also propose the same protocols on our experimental Zeronet, where one voting period lasts 56 hours and the whole voting procedure can be carried out in 9 days. This gives us the opportunity to test the whole procedure and to analyze the activation of a new protocol in Zeronet.

The actual Athens proposal is a self-contained set of OCaml sources. To make it more readable, we have split it into a clean series of patches on top of the current protocol. This article will describe each of these patches.

We recommend that users take the time to read the actual code and make sure that it correctly reflects the advertised behavior.

For this first update, we decided to include an example of invoicing, an on-chain way to fund the development of protocol proposals. During the migration, the account of the authors of a proposal can be funded by creating a certain amount of tez. In this case, we chose a symbolic value of 100ꜩ, enough to buy a round of drinks for the devs who worked on these proposals. In the future, we hope this mechanism can be used to fund the work of new teams and help increase decentralization.

The next section presents a set of benchmarks for the gas and the migration from protocol Alpha_003 to Athens_004.

Building confidence in a protocol amendment
Tezos has a built-in system to try new protocols: it can fork a test chain from the main chain. The test chain has the advantage of forking from an actual context of mainnet, complete with all the accounts and contracts, thus giving a realistic trial of what could happen at activation.

Additionally, it will be possible to try out the voting procedure and the new protocol on the Zeronet development network. This test will be public, it will run on a reduced timescale as cycles are shorter in Zeronet, so it will be a good way to demonstrate the whole process. We will also inject our proposals on Zeronet, and while on Mainnet the proposal period will last 3 weeks, on Zeronet it will take just 56 hours. If quorum and super-majority are reached, Zeronet could see a protocol amendment in as little as 9 days.

Prior to these large scale tests, we performed various tests and benchmarks to make sure not only that the new protocol is behaving correctly but also that the migration from the old protocol runs in a reasonable amount of time and disk space.

We will now give a short overview of three aspects that we thoroughly tested before proposing the update: the gas limit update, the roll size update, and the reindexation of accounts.

Gas Limits
The new gas limits were decided by measuring the validation time of worst case blocks (with a large number of computation intensive or IO intensive contract executions), in a realistic mainnet context.

The cost of IOs was slightly underestimated, and thus the balance between IOs and computation has been adjusted in this update. As a result, the number of IOs performed in a block stays unchanged, while the number of computation steps is doubled.

These new limits are kept within conservative bounds, allowing for a smooth operation of the network.

The Michelson benchmarking effort is continuing, in order to converge to a more refined gas accounting scheme that should allow us to propose a less conservative limit in a future upgrade.

The benchmarking method we are using will be the topic of a forthcoming series of blog posts.

Roll size migration
We simulated the upgrade in the current mainnet state, with and without the roll size update, and measured the difference in validation time and disk storage.

We tested two versions of the state, a full context of 72GB and a snapshot generated context of 330MB (snapshots are a recently introduced feature). In addition the migration was tested on Solid State Drives (SSD) and Hard Disk Drives (HDD).

In a snapshot generated context, the roll size update increases the migration time by less than 10 seconds on both SSD and HDD, while the disk usage is increased by about 130MB. In a full context, the migration time is increased by 9.5 seconds on SSD and 26 seconds on HDD. The disk usage is increased by 340 MB.

This cost only affects the migration block and has no impact on the subsequent operation of the network.

In our opinion, the performance difference between the two protocol proposals is not significant.

The use of more efficient storage back-ends that we are currently evaluating, shows even better performance in both time and disk usage.

Contract reindexation
The new uniform indexing of contracts and accounts has no effect on the behaviour of the protocol. It is a preliminary to future improvements of the account system, such as making key handling more flexible for bakers.

Migration from the current to the new internal structure presents a noticeable cost in terms of time and disk that we carefully benched to make sure that the overhead only affects the migration block and has no impact on the subsequent operation of the network.

Using the current node implementation with a full mainnet context, the migration takes about two minutes and increases disk occupancy by around 5%. Running the same migration in a shallow snapshot reduces both time and disk usage by an order of magnitude.

A note on disk usage and migration time
As numbers above show, nodes bootstrapped from a snapshot, running with partial archives, show much better performance. Snapshots are the first step toward better storage that we want to integrate into the node in the next few weeks. The preliminary tests of more efficient experimental storage back-ends, that are the next step, show even better performance in disk usage.

We kindly recommend that node administrators, in particular users of magnetic disks for whom the migration will be heavier, give a try at snapshots on a secondary machine, to help us speed up their integration in the mainnet branch.

Changelog
The full changelog of patches applied to the protocol is presented in the following section. The main changes consist in the invoicing, the changes to gas limits and the change in rolls size which applies to one single proposal. The remaining commits consist in improvements or corrections.

Main changes
Alpha/Michelson: double gas limit, except for I/Os that stay unchanged

This change doubles the maximum amount of gas for the block and for each transaction. The cost of I/Os is updated accordingly, so that the number of computation steps is increased, while the number of possible I/Os in a block stays unchanged.

Alpha/Michelson/Migration: double hard_gas_limit_per_{operation,block}

This patch is part of the migration code from alpha_003 to athens_004.

It performs the migration of the constants in mainnet to update them to the new (doubled) default values.

Alpha/Delegation: set the tokens_per_roll constant to 8000 tez

This patch sets the new default roll size to 8000 tez.

only in one proposal

Alpha/Delegation/Migration: reassign rolls

This patch is part of the migration code from alpha_003 to athens_004.

It performs the roll size update to 8000 tez.

only in one proposal

Alpha/Migration: invoicing example

This patch is part of the migration code from alpha_003 to athens_004.

It credits the account of the authors of this protocol with a symbolic value of a hundred tez.

The authors hereby declare that these tez will be used exclusively to pay for a round of drinks for the Nomadic team.

Other changes
Alpha/Refactor: remove unused compare module in constants

This commit removes a small piece of unused code.

Alpha/Refactor: use return_*

Replaces some allocations of common return values by preallocated constants. This is a cosmetic change that does not alter the semantics.

Alpha/Refactor: simplify code of Raw_context.prepare_first_block

This patch simplifies the protocol migration code. It does not change the semantics.

Alpha/RPCs: fix wrong encoding name fields

This patch fixes a mislabeled field in the JSON encoding of operation receipts. The balance updates had a field level that actually contained a cycle. It is now named cycle.

Third party software that make use of receipts such as block explorers or wallets may need to be updated accordingly. A simple solution is to support both during the protocol change period.

Alpha/RPCs: change managerPubkey to underscores in json field names

This renames a field in the JSON format of origination operations to make it consistent with the others.

Third party software must be updated to support this change. For applications that just consume this format, it is enough to just recognise both field names during the protocol change period. For applications that send this format to the node, it is necessary to send the right one depending on the next_protocol field of the interrogated block.

Alpha/RPCs: rename field labels in Michelson typechecker output

This is a change in field names in the Michelson typechecking RPC output. A simple solution for third party tools to handle the change is to recognise both field names during the protocol update period.

Alpha/RPCs: rename Michelson interpreter diagnostics field names

This patch changes some error IDs and error attributes to make it consistent with the rest of the codebase.

Third party development tools that recognise errors based on this should be updated.As errors are only read, a simple solution is to support both during the protocol change period.

Alpha/RPCs: optional fields for source, payer, gas in run and trace

This patch adds new options to the Michelson interpreter RPCs. They allow customization of interpreter behaviour. It only makes utility RPCs more flexible, and does not change the semantics of Michelson.

Alpha/Vote: fix initialization of voting rights from genesis

This patch fixes the voting system initialisation from genesis.

It improves voting in sandbox mode and test networks. It has no effect on mainnet.

Alpha/Vote: docstrings and one renaming

This patch adds documentation to the code. It does not change the semantics.

Alpha/Vote: fix potential overflow in quorum computation

This patch prevents a potential overflow if the number of rolls gets too big in a future update. This bug cannot happen with the current value.

Alpha/Vote: new constant test_chain_duration

This patch turns the test chain lifetime from a hardcoded two-days constant in the code to a parameter. This is useful for testing of the voting system, for testnet rehearsals, or for more easily changing the value in a future protocol upgrade.

The value of this constant is also updated to prolong the test chain to the end of period. The test chain is now spawned at the beginning of the third voting period and lasts the whole period.

Alpha/Vote/Migration: update constants binary representation

This patch is part of the migration code from alpha_003 to athens_004.

Since we changed Constants_repr.parametric_encoding for adding the new constant test_chain_duration, we need to upgrade the content of the constants_key in the context.

Alpha/Michelson: allows big maps in genesis blocks

This patch allows to create a sandbox chain with bootstrap contracts that use big_maps. This change is mostly useful for testing in sandbox mode or custom deployments.

This has no effect on mainnet.

Alpha/Michelson: do not allow spendable smart contracts

This patches forbids the creation of spendable smart contracts, a current feature that is not necessary (it can be implemented from the contract’s code) and prevents some possible future optimisations.

It also prevents the creation of non spendable non scripted contracts, which was mostly a design flaw that could let people inadvertantly lock funds.

This is done by checking the flag in the origination operation rather than removing it so the change is lighter for third party software.

Alpha/Delegation: do not activate delegate when crediting

Remove the unneeded method for a delegate to reactivate itself by provisioning its account. The delegate should simply re-register as delegate.

Alpha/Delegation: allow to change the roll value

This patch adds a function to reassign rolls given a new roll size.

Alpha/Contracts: uniform indexation for both implicit and originated contracts

This patch change the way contracts are indexed internally, using the same database paths for implicit contracts, delegate accounts, scripted and non scripted contracts.

It simplifies future updates, for instance to add new account types.

Alpha/Contracts/Migration: reindex contracts

This patch is part of the migration code from alpha_003 to athens_004.

It performs the migration of contracts to the new uniform storage format.
