.. _arluckathens:

----------------------------


Arluck Athens
============================



Jacob Arluck
Feb 25
Posted on Medium 

An overview of Athens, the first Tezos on-chain upgrade proposals


This post summarizes today’s announcement from Nomadic Labs regarding Athens, the first on-chain Tezos upgrade proposals for Tezos arriving later this week. For a more technical explanation and changelog, please read Nomadic’s post directly.

And of course, for a full overview of the Tezos governance mechanism, please check out “Amending Tezos” and the visualization below:


What’s in the Athens proposals?
The proposed changes in the two Athens proposals are incremental:

Athens Proposal 1: increase the gas limit

The gas limit was set conservatively at launch to protect the chain, with the idea that it could be increased via the amendment process
This proposal would increase the gas limit by allowing double the computation steps in each block while keeping the number of IOs performed in each block unchanged
This will make it incrementally easier to deploy smart contracts on Tezos, but maintains the gas limit at a conservative level as Nomadic Labs awaits more detailed benchmarking results
Nomadic Labs aims to propose further gas limit increases and a re-weighting based on ongoing benchmarking efforts
Athens Proposal 2: increase the gas limit (see Proposal 1) AND reduce the roll size to 8,000 XTZ for Tezos Proof-of-Stake

In current Tezos consensus, a baker must have 10,000 XTZ to be randomly selected to propose or endorse a block
In addition to increasing the gas limit, this amendment would reduce that threshold to 8,000 XTZ, incrementally lowering the barrier to entry to baking
Please note that beyond these two incremental changes, the upgrade proposals also include minor fixes which do not significantly change the protocol, as described in the changelog.
3.18
Benchmarking Athens
Nomadic states in their announcement post that they performed extensive testing to ensure the proposed protocols behave as intended and that the migration to Athens would not use an overly burdensome amount of time and disk space.

For the gas costs, Nomadic based the new gas limits on a realistic mainnet context, with the new limits still set conservatively to ensure the network runs smoothly.

Outside of the proposals, earlier this month Nomadic announced a new feature for Tezos nodes to improve storage via “Snapshots” and “History Modes”, as detailed in this blog post. As noted in today’s post, Nomadic hopes to integrate the snapshot feature into Tezos nodes within the next few weeks, improving storage performance.

Nomadic tested the effects of roll size reduction for both kinds of nodes: full nodes adopting the snapshot feature (allowing faster sync and less storage) as well as nodes with a full context (i.e. “archive” nodes maintaining history of all past states).

Regarding the proposals more broadly, Nomadic reports there is a one-time increase in disk space and time when the network migrates from the old protocol to the proposed Athens protocol.

Based on these results, Nomadic deems any performance differences between the two proposals inconsequential:

This cost only affects the migration block and has no impact on the subsequent operation of the network. In our opinion, the performance difference between the two protocol proposals is not significant.
As implied, it is most important to evaluate proposals based on their longer-term economic impact (e.g. the cost of baking, barrier to entry, etc.) and operational impact (e.g. does the network continue running smoothly?), rather than a one-time cost of migration.

Stakeholders will be able to evaluate this for themselves in the upcoming Zeronet testing initiative described in the “What comes next?” section below.

Invoicing
As part of the upgrade, Nomadic will include a modest invoice for 100 XTZ:

For this first update, we decided to include an example of invoicing, an on-chain way to fund the development of protocol proposals. During the migration, the account of the authors of a proposal can be funded by creating a certain amount of tez. In this case, we chose a symbolic value of 100ꜩ, enough to buy a round of drinks for the devs who worked on these proposals. In the future, we hope this mechanism can be used to fund the work of new teams and help increase decentralization.
Why such modest, incremental changes?
As described in Nomadic’s first “Meanwhile at Nomadic” post in January:

The main goal [of the first proposals] is to prove that Tezos can carry out a successful update both technically and as a community. After all, we are all novices to the Tezos governance procedure and this aims to be a first step towards more substantial amendments.
As stated, the goal is to demonstrate the mechanism first before advancing to shinier, more exciting upgrades which might lay over the horizon. A 3-month upgrade timeline enables Tezos to make exciting advances incrementally and a multi-stage voting mechanism aims to ensure the accountability of proposed changes to the network.

Why “Athens”?
In last week’s post, Nomadic Labs proposed a new naming convention for Tezos proposals:

Our suggestion is to use city names, anglicised and in alphabetic order. Of course, in the future other entities will propose protocol upgrades, and it will be up to them to follow the convention or not. Yet, city names provide a wide set to choose from for each letter, with even a bit of room to express things.
As a demonstration, “Athens” is a pretty obvious choice for this first voted upgrade, don’t you think?
For illustration purposes, one might imagine future upgrades named “Brasília”, “Canberra”, “Delhi”, and so on.

Note: the name “Athens” is not intended to bear any relation to Ethereum’s Constantinople hard fork also scheduled for later this week.

What comes next?
Protocol hashes, injection, and voting
Later this week, Nomadic aims to provide the protocol hashes and inject the proposals. Bakers will then have roughly 3 weeks to upvote the Athens proposals, with the top-ranked proposal proceeding to a “Yes/No” exploratory vote for the subsequent 3 weeks.

Voting instructions
Nomadic will publish instructions for bakers on how to vote. The Tezos community should also expect to see detailed instructions from Obsidian Systems regarding how to use Ledger for voting.

Zeronet
The Tezos Zeronet will be reset to offer a public test network with accelerated voting periods each lasting 56 hours long. Using the Zeronet, the entire voting procedure can be tested over 9 days, allowing stakeholders to analyze the activation of a new protocol safely within the test environment.

Signal Your Preference and Join the Conversation


tezvote.com
To signal your preference or to check your baker’s preference ahead of the Athens vote, please check out Stephen Andrews from TezTech’s’ TezVote, a recently launched vote signaling tool based on a simple Michelson contract.

Upon announcement of the first Tezos proposals last month, preliminary discussions began on Kialo. It is worth noting that new evidence will become available once the proposals are injected and entities independent of Nomadic evaluate the proposals.

Expect to see renewed discussion there and join the conversation!
