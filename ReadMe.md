# Gitlab hosting

Each new commit in the ```master``` branch will run a CI pipeline. 
Said pipeline will deploy the most current version to gitlab hosting.
The deployment process can take a while ```5-20min```.